package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string
var authToken string

var rootCmd = &cobra.Command{
	Use: "nexusmods",
	Short: "Command for uploading mods to Nexus Mods",
	Long: `Tool to provide an unofficial way to automate the
uploading of mod files on Nexus Mods`,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.nexusmods-uploader.yaml")

	rootCmd.PersistentFlags().StringVar(&authToken, "authkey", "", "NexusMods API Token")

	viper.BindPFlag("authkey", rootCmd.PersistentFlags().Lookup("authkey"))

	rootCmd.AddCommand(uploadCmd)
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory
		home, err := os.UserHomeDir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".nexusmods-uploader")
	}

	viper.AutomaticEnv() // Read in environment variables that match

	// If a config file is found, read it
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
