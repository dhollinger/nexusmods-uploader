package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	uploadCmd = &cobra.Command{
		Use: "upload",
		Short: "Upload files to Nexus Mods",
		Long: `Upload files to Nexus Mods`,
		Run: upload,
	}
	options struct{
		gameId string
		modId string
		name string
		version string
		description string
		filePath string
	}
)

func init() {
	uploadCmd.PersistentFlags().StringVarP(&options.gameId, "game_id", "g", "", "Nexus Mods game id")
	uploadCmd.PersistentFlags().StringVarP(&options.modId, "mod_id", "m", "", "Nexus Mods mod id")
	uploadCmd.PersistentFlags().StringVarP(&options.name, "name", "n", "", "Mod name")
	uploadCmd.PersistentFlags().StringVarP(&options.version, "version", "v", "", "Mod file version")
	uploadCmd.PersistentFlags().BoolP("is_latest", "l", false, "Is upload the latest (Default: false)")
	uploadCmd.PersistentFlags().BoolP("is_new_of_existing", "e", false, "Is the newest of an existing file (Default: false)")
	uploadCmd.PersistentFlags().StringVarP(&options.description, "description", "d", "", "Description of the upload")
	uploadCmd.PersistentFlags().StringVarP(&options.filePath, "file_path", "p", "", "Path of the file to upload")

	rootCmd.AddCommand(uploadCmd)
}

func upload(cmd *cobra.Command, args []string) {
	if _, err := os.Stat(options.filePath); os.IsNotExist(err) {
		fmt.Println("File does not exist:", err)
		os.Exit(1)
	}

	token := viper.Get("auth_token")

	if token == "" {
		fmt.Println("Nexus Authentication Token required")
		os.Exit(1)
	}


}
